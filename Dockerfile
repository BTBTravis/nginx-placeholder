FROM nginx
ENV AUTHOR=Docker

WORKDIR /usr/share/nginx/html
COPY example.html /usr/share/nginx/html

CMD cd /usr/share/nginx/html && sed -e s/Docker/"$AUTHOR"/ example.html > index.html ; nginx -g 'daemon off;'


